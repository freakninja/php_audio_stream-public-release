# Public Release

General purpose audio streaming library

# USAGE #

```php
$file_path = realpath('path/to/file');
require ('Http/Audio/Stream.php');
$http_audio_stream = new \Http\Audio\Stream($file_path);
$http_audio_stream->enable_audio_transcoding = !preg_match('/\.wav$/', $file_path);
$http_audio_stream->play();
```