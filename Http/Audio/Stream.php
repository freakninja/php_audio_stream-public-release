<?php
namespace Http\Audio;

class Stream 
{
	/**
	 * format wave
	 * 
	 * @var string
	 */
	const FORMAT_WAVE = 'wav';
	/**
	 * format mp3
	 * 
	 * @var string
	 */
	const FORMAT_MP3  = 'mp3';
	/**
	 * mp3 transcode command
	 * 
	 * @var string
	 */
	const TRANSCODE_MP3 = '/usr/bin/sox %s -b 8 -r 8000 -t wav %s ';
	
	/**
	 * false, error_reason in case of error 
	 * @var string
	 */
	public $error = false;
	/**
	 * file extension
	 * 
	 * @var string
	 */
	public $file_ext = null;
	/**
	 * current file path
	 * 
	 * @var string
	 */
	public $file_path = null;
	/**
	 * file name
	 * 
	 * @var string
	 */
	public $file_name = null;
	/**
	 * file size
	 * 
	 * @var integer
	 */
	public $file_size = 0;
	/**
	 * if set, file path to transcoding output
	 * 
	 * @var string
	 */
	public $transcoding_file = null;
	/**
	 * $_SERVER variable
	 * 
	 * @var array
	 */
	public $http_context = null;
	/**
	 * Response Content-Length
	 * 
	 * @var integer
	 */
	public $http_content_length = 0;
	
	/**
	 * Response Content-Type
	 * 
	 * @var string
	 */
	public $http_content_type = 'audio/x-wav';
	
	/**
	 * used for chunking, start offset
	 * 
	 * @var int
	 */
	protected $_seek_start = null;
	/**
	 * used for chunking, stop offset
	 * 
	 * @var int
	 */
	protected $_seek_end = null;
	
	/**
	 * true if file transconfig is required 
	 * 
	 * @var bool
	 */
	public $enable_audio_transcoding = true;
	
	/**
	 * set class in debug mode
	 * 
	 * @var bool
	 */
	public $debug = false;
	/**
	 * class constructor
	 * 
	 * @param string $file_path
	 * @return void
	 */
	public function __construct($file_path)
	{
		global $_SERVER;
		
		if(!$file_path || !file_exists($file_path))
			return $this->_response_header("HTTP/1.0 404 Not Found", true);
		if(preg_match('/\.wav$/i', $file_path)) {
			$this->file_ext = self::FORMAT_WAVE;
		} else if ( preg_match('/\.mp3$/i', $file_path)) {
			$this->file_ext = self::FORMAT_MP3;
		} else {
			return $this->_response_header('HTTP/1.0 500 Internal Server Error', true);
		}
		
		$this->file_path = $file_path;
		
		$this->http_context = $_SERVER;
		
		$this->file_name = basename($file_path);
		
		$this->file_size = filesize($file_path);
		
		if ( $this->file_size == 0 ) {
			return $this->_response_header("HTTP/1.0 404 Not Found", true);
		}
	}
	
	/**
	 * perform transcoding file cleanup
	 * 
	 */
	public function __destruct()
	{
		
		if($this->transcoding_file && file_exists($this->transcoding_file)) {
			unlink($this->transcoding_file);
		}
	}
	
	/**
	 * perform file transconfig
	 * 
	 * @return boolean|string file path if successful, false otherwise
	 */
	protected function _transcode()
	{
		if($this->error) 
			return false;
		
		if(!$this->enable_audio_transcoding)
			return $this->file_path;
		
		switch($this->file_ext) {
			case self::FORMAT_WAVE:
				return $this->file_path;
				break;
			case self::FORMAT_MP3:
				$transcondig_file = '/tmp/' . basename($this->file_path, '.mp3') . '.' . self::FORMAT_WAVE;
				if(!file_exists($transcondig_file) || filesize($transcondig_file) == 0) {
					$command = sprintf(self::TRANSCODE_MP3, $this->file_path, $transcondig_file);
					$result = shell_exec($command);
					if(!file_exists($transcondig_file) || filesize($transcondig_file) == 0) {
						return $this->_response_header('HTTP/1.0 500 Internal Server Error', true);
					}
				}
				if(file_exists($transcondig_file) && filesize($transcondig_file)) {
					$this->transcoding_file = $transcondig_file;
					$this->file_size        = filesize($this->transcoding_file);
					$this->file_name        = basename($transcondig_file);
					return $this->transcoding_file;
				}
				break;
		}
		return $this->file_path;
	}
	
	/**
	 * setup reply headers
	 * 
	 * @return void
	 */
	protected function _headers()
	{
		@apache_setenv ( 'no-gzip', 1 );
		@ini_set ( 'zlib.output_compression', 'Off' );
		// set the headers, prevent caching
		$this->_response_header ( "Pragma: public" );
		$this->_response_header ( "Expires: -1" );
		$this->_response_header ( "Cache-Control: public, must-revalidate, post-check=0, pre-check=0" );
		$this->_response_header ( 'Content-Disposition: inline;' );
		$this->_response_header ( 'Content-Transfer-Encoding: binary' );
		$this->_response_header ( "Content-Type: {$this->http_content_type}");
		
		// Only send partial content header if downloading a piece of the file (IE workaround)
		if ($this->_seek_start > 0 || $this->_seek_end < ($this->file_size - 1)) {
			$this->http_content_length = ($this->_seek_end - $this->_seek_start + 1);
			$this->_response_header ( 'HTTP/1.1 206 Partial Content' );
			$this->_response_header ( "Content-Range: bytes {$this->_seek_start}-{$this->_seek_end}/{$this->file_size}" );
		}
		
		$this->_response_header ( "Content-Length: {$this->http_content_length} ");
		$this->_response_header ( 'Accept-Ranges: bytes' );
	}
	
	/**
	 * set a response header
	 * 
	 * @param string $header
	 * @param bool $close_connection
	 */
	protected function _response_header($header , $close_connection = false)
	{
		if($this->debug)
			print($header . PHP_EOL);
		else
			header ( $header );
		if($close_connection) 
			exit;
	}
	
	/**
	 * print a file chunk 
	 * 
	 * @param string $file_path
	 * @return boolean
	 */
	protected function _file_chunk($file_path)
	{
		$seek_start = $this->_seek_start;
		$seek_end   = $this->_seek_end;
		set_time_limit ( 0 );
		$file_handle = fopen($file_path, 'r');
		fseek ( $file_handle, $seek_start );
		while ( ! feof ( $file_handle ) ) {
			print (@fread ( $file_handle, 1024 * 8 )) ;
			ob_flush ();
			flush ();
			if (connection_status () != 0) {
				@fclose ( $file_handle );
				exit ();
			}
		}
		@fclose ( $file_handle );
		return true;
	}
	
	/**
	 * transcode and play an audio file
	 * 
	 * @return boolean
	 */
	public function play()
	{
		$file_path = $this->_transcode();
		return $this->stream($file_path);		
	}
	/**
	 * stream a file chunk
	 * 
	 * @param string $file_path
	 * @return true
	 */
	public function stream($file_path = null)
	{
		if($file_path == null)
			$file_path = $this->file_path;
		
		if($this->error || !$file_path) {
			return $this->_response_header("HTTP/1.0 400 Bad Request", true);
		}
		
		$this->http_content_length = filesize($file_path);
		$this->http_content_type   = mime_content_type($file_path);
		$this->file_size           = filesize($file_path);
        
		// check if http_range is sent by browser (or download manager)
		if (isset ( $this->http_context ['HTTP_RANGE'] )) {
			list ( $size_unit, $range_orig ) = explode ( '=', $this->http_context ['HTTP_RANGE'], 2 );
			if ($size_unit == 'bytes') {
				// multiple ranges could be specified at the same time, but for simplicity only serve the first range
				// http://tools.ietf.org/id/draft-ietf-http-range-retrieval-00.txt
				list ( $range, $extra_ranges ) = explode ( ',', $range_orig, 2 );
			} else {
				$range = null;
				return $this->_response_header( 'HTTP/1.1 416 Requested Range Not Satisfiable' , true);
			}
		} else {
			$range = null;
		}
		
		// figure out download piece from range (if set)
		@list ( $seek_start, $seek_end ) = @explode ( '-', $range, 2 );
		
        // set start and end based on range (if set), else set defaults
		// also check for invalid ranges.
		$this->_seek_end = (empty ( $seek_end )) ? ($this->file_size - 1) : min ( abs ( intval ( $seek_end ) ), ($this->file_size - 1) );
		$this->_seek_start = (empty ( $seek_start ) || $this->_seek_end < abs ( intval ( $seek_start ) )) ? 0 : max ( abs ( intval ( $seek_start ) ), 0 );
		
        // setup response headers
		$this->_headers();
		
        // return the file content
		$this->_file_chunk($file_path);
		
        return true;
	}
}